# Ciao!

Je profite de ce *Ciao!* pour te donner un peu de matière, avant d'aller sur autre chose.

## Musique

- [Farewell Transmission](https://www.youtube.com/watch?v=tNy-sqj7BCc) (YouTube) - [ref](https://www.discogs.com/fr/master/186370-Songs-Ohia-The-Magnolia-Electric-Co)

## Au début

Au début, je me souviens, la boîte avait un look vivace, bien cool, bien sympa ; j'ai donc postulé. Ça paraît bête mais c'est déjà important. Le côté cool, sympa, vivace, frais : ça attire. C'est si rare dans les entreprises.

D'ailleurs, c'est ce même côté sympa / cool / humain, qui est présenté aux apprenants sur les pages du blog de la boîte. Et ça, ça les attire également.

Donc, au début, j'ai postulé pour cette boîte sympa et humaine. Je me suis défoncé pour passer les 5 étapes de recrutements (dont un projet web de "Memory" que j'ai volontairement "kitschisé" en exploitant des vieilles musiques midi, que j'ai transcodé en mp3, bon délire).

C'est lors de ces 5 étapes que j'ai rencontré ce que j'appellerai le *responsable pédagogique*. J'ai discuté avec cette personne pendant plusieurs heures (oui, *plusieurs* heures) pour échanger des points de vues, des visions, des *valeurs*. Et c'est ce qui m'a tout de suite impliqué encore plus dans la boîte : les valeurs sur lesquelles on venait d'appuyer durant ce call (call faisant parti du process normal de recrutement à ce moment). Implicitement, je savais que j'allais devoir me décarcasser pour *prendre le temps* d'expliquer à mes futurs apprenants des tas de notions. Et que j'allais aussi devoir prendre le temps *d'avoir du recul* sur mes propres méthodes d'enseignements. Acquérir une sorte de philosophie autour de la pédagogie. Je savais que mes méthodes pédagogiques allaient être passées au crible par ce même personnage *central*, ce *responsable pédagogique*, si soucieux de ce que j'appellerai la **qualité humaine** et **pédagogique** de l'école. J'insiste sur ce point, parce que ce fondement a été disloqué. J'y reviendrai.

### Visio-cours

Premiers cours donnés dans l'outil de "visio-cours", premières joies, premières peines ; C'est là que je me suis rendu compte que ça foutait vachement plus le trac d'être prof dans une salle virtuelle, que dans une salle réelle (j'ai été formateur DWWM IRL durant 4 ans auparavant).

Premiers cours et premiers coups de stress. J'ai même payé mon superfail sur une saison 4 ; pensant qu'une préparation légère et beaucoup d'improvisations suffiraient. Erreur monumentale.

Mais cette erreur fût *rapidement* désamorcée par le *responsable pédagogique* avec lequel je m'entretenais directement **par messages privés**. Ouais, à cette époque, c'était encore possible, on se parlait directement. Normalement. Sans chichis hiérarchiques tout pourraves. Je précise, parce que ce rapport à radicalement changé. Parce que le "responsable pédagogique" (ou assimilé) est parti et qu'il a été remplacé par une toute autre personne ; mais là, pareil, j'y reviendrai.

### Le responsable pédagogique veillait au grain

Le responsable pédagogique m'a permis de prendre du recul sur mes erreurs, et, exactement comme nous, les profs, on le dit aux étudiants : **l'erreur, c'est la plus grande porte ouverte à l'apprentissage**. Donc j'ai, avec lui, améliorer mes méthodes pédagogiques. Je me suis gratté le cerveau, et j'ai expérimenté des nouvelles façons de préparer des cours.

Le verdict fut sans appel, il me fallait **2 à 3 jours ouvrés de temps de préparation**, pour **une seule journée de cours jamais donnée**. C'est ce qui me permettait de préparer des vrais cours de qualité.

À cette époque, je pouvais avoir ce temps de préparation. Et à cette époque, les commentaires ultra positifs des apprenants ont commencé à tomber. Et quelle satisfaction de les voir ... satisfaits ! Et je ne vais pas te l'apprendre, dans le commerce, on sait qu'un client satisfait amène d'autres clients qui seront satisfaits à leur tour. Etc. Toi même tu sais. Mais ... notre direction l'aurait-elle oubliée, parmi ses tableaux et ses "indicateurs" hors sols ?

### La création vertueuse

J'ai vraiment commencé à cravacher grave. Et je passais de plus en plus de *temps* à m'approprier les cours, en *créant* des supports que j'estimais soit manquants, soit insuffisants, dans les trames des programmes. Tous ces supports que je créais, je les partageais directement avec les collègues, qui les utilisaient à leur tour dans leurs cours, et qui m'en disaient le plus grand bien. Et qui m'en disent encore aujourd'hui du bien, d'ailleurs.

J'ai ensuite créé des exos supplémentaires, qui plaisent aussi. Puis de nouveaux ateliers, qui plaisent, puis de challenges supplémentaires d'entraînement, dont raffolent les apprenants.

Ah, la belle époque, où on avait encore le temps de faire les choses correctement...

### Les néophytes n'ont rien à faire là

En réussissant à donner toutes les saisons du programme sur lequel j'étais, je me suis dit : Wow, cette formation est vraiment d'un niveau sensationnel. On y apprend vraiment des tas de trucs. Des tonnes de bonnes pratiques.

Mais, bordel, qu'est-ce qu'elle est dense cette formation ! Certains jours, j'ai l'impression de travailler dans le "foie gras de la connaissance". Où les oies seraient nos étudiants, nos étudiantes. Tu trouves cette image hardcore ? Alors essaye de côtoyer d'un peu plus prêt les apprenants actuels, tu verras.

**Non, cette formation, en l'état, n'est pas adressée à des néophytes.** J'aime souvent imaginer mon père en tant qu'apprenant, et je me pose la question suivante : est-ce que ce cours lui serait adapté ? Dans le cas de nos formations, malgré ce qui est dit en façade pour attirer les clients/apprenants, la réponse est non. Juste non. **Non, ces programmes, en l'état, ne sont pas adaptés à des apprenants qui n'ont jamais codés de leur vie.**

Je vous l'ai dit, auparavant j'ai travaillé sur le titre DWWWM. J'ai taffé avec des publics très divers : RSA, ex boulanger, SDF, infra bac, hardcore gamers livides au petit matin, etc. Mais dans cette vie antérieure, j'avais la chance de pouvoir suivre une promo du début à la fin, jusqu'au titre. Et je connaissais de beaux fragments de vie, des fragments d'histoire de chacun/chacune. Je **voyais** *qui* était en difficulté, et à *quel moment*, en **"live"**. Et je pouvais moduler mon cours, et me remettre en question, pour l'adapter au mieux à ces personnes en difficulté. J'avais **le temps** de les suivre pendant plusieurs mois. Et je voyais, physiquement, lorsqu'ils étaient en difficulté.

Bref, dans notre école, ça n'est pas la même : les programmes sont en béton armé, c'est du lourd, ça envoie du lourd, les apprenants prennent cher, on voit des tonnes de notions en un temps *compressé à la race*, donc, **forcément inadapté aux novices**. Et ces tonnes de notions se doivent d'être extrêmement bien préparées en amont, ne serait-ce que pour limiter la casse.

### Préparer c'est optimiser

Pourquoi faut-il bien préparer ces notions en amont, et donc avoir le temps de les préparer ?

1. Premièrement parce que les notions et objectifs pédagogiques ne sont pas clairement identifiées et mis en avant dans les trames. Ça fait au moins 2 ans que c'est le cas. Ça n'évolue que trop faiblement. Donc c'est à nous les profs de les identifier, structurer, organiser. En amont. Et ça prend du temps.
2. Deuxièmement, parce que les journées de cours en "visio cours", censées se terminer à 15h maximum (en tout cas c'est ce qui est communiqué aux élèves), peuvent très vite se terminer à 16h, voir 17h ou plus. Donc au prof encore un coup de se taper le boulot et d'optimiser ses cours au mieux, en amont, et ça, ça prend aussi du temps.

### Apprenants .. ou spectateurs

L'apprenant - ayant pour la plupart du temps assisté au cours comme quasi simple spectateur, pouvant juste "voter" chaque 1, 2 ou 3 heures (via des "sondages"), ou proposer des fragments de code dans le "chat" - termine trop souvent sa journée la tête HS, surchargée d'informations, et pire que tout : sans avoir pu s'exercer une seule fois de la journée !

**Essayez juste d'imaginer ça dans la vie réelle.** Un prof qui parle qui parle qui parle, **qui parle encore**, qui propose une pause, qui parle, qui demande si vous avez compris, qui parle, puis **qui parle encore** et encore et qui code et qui parle et qui code et qui parle. Puis ce prof finit par vous donner un exercice, bien hardcore, à la fin de la journée. Vous savez, pile-poil quand vous n'avez plus de cerveau. Honnêtement, vous trouverez ça pédago-quelque chose, vous ?

> Voir un prof gesticuler pendant 6 heures, ok, mais tant qu'on a pas soit-même mis les mains dans le cambouis, on ne se rend pas compte. On a la sensation de ne pas savoir faire. Et ce syndrome ne nous lâche pas jusqu'à la fin de la formation.

## Musique

- [Waiting for Surprises](https://www.youtube.com/watch?v=6xyYGPdwKQ0) (YouTube) - [ref](https://www.discogs.com/fr/master/168756-Baxter-Dury-Floor-Show)

### Préparer les cours CORRECTEMENT, ça prend tellement de TEMPS

Donc plus je préparais mes cours, et plus les cours étaient concis, efficaces, *"straight to the point"*. Parce que chaque notion y était identifiée, abordée, disséquée, sondée, commitée. Un vrai kif. Mais un gros taf de préparation. **Préparer des cours correctement ça prend du temps.** Mais tu l'as compris maintenant, pas vrai ?

> Purée, j'adore tes cours 😍

> Si un jour je passe prof, j'essayerai de me souvenir de ta façon de faire. Franchement.

> Honnêtement, pédagogie au top, merci

Dans mes cours, tout était préparé à l'avance : les intro, les outros, le moindre morceau de code, les gros commentaires reformulés 100x pour être pertinents, les slides, les exos, les sondages, les messages de commit, toutes les phases qu'on peut passer en "code review" pour gagner du temps. Tout. Bon, tout, sauf les blagues, soyons sérieux.

> Bonjour je me permets de vous envoyer ce message car je suis dans l'autre cockpit de la promo mais malgré ça je suis vos cours tous les soirs et ça m'aide énormément j'aime beaucoup votre pédagogie vos allez droit au but alors même si je suis pas avec vous je tenais à vous remercier pour vos cours, pour le soutien que vous me donnez merci beaucoup d'être là et d'avoir la pêche

> Tu as été un super prof, le rythme des cours était parfait, super bien animés donc beaucoup plus facile d'appréhender les notions sans que ça devienne ennuyeux, tout est super clair. [...] Finir plus tôt m'a particulièrement aidée sur les notions, la théorie ne voulant pas rentrer. Le temps [...] qui nous a été offert j'ai pu l'utiliser pour pratiquer et comprendre de moi-même [ce temps, ndr] n'était pas perdu bien au contraire

### Donner du sens à ce qui est fait

Bref c'était une époque passionnante, une époque où nous les profs on travaillait comme des malades, mais une époque où ce travail était fait dans la joie, malgré tout. Parce que non seulement on avait *le temps* de préparer des cours de qualité, mais aussi parce qu'on avait *le temps* d'en tirer une certaine forme de satisfaction, après les cours : sur les channels de promo, sur un canal de détente ou de faux potins à l'humour débridé.

On avait *le temps* de prendre du recul par rapport à ce qu'on faisait. On avait *le temps* de donner du sens à ce qu'on faisait.

> OMFG, on est tellement à des années-lumières de cette époque. Pourtant c'était y'a quoi ... y'a même pas deux ans non ? Que s'est-il passé ?

## La bascule

À quel moment ça a basculé ?

### CEO

Je n'ai jamais vraiment ressenti une forte présence du CEO dans la boîte. On l'entraperçoit sur notre outil de communication, il ne dit pas bonjour dans les canaux sur lesquels on est invité à le faire. Est-il connecté, oui, non.

Avant, quand tout allait bien, du côté des formateurs, ça n'était pas un problème. Chacun sa vie, le CEO fait ce qu'il a à faire, et il s'applique probablement autant que nous dans son travail. De temps en temps, on croisait un message de sa part sur un canal de communication, message accompagné de centaines d'émojis, de licornes, annonçant une info, souvent enjolivée en mode bonne nouvelle, blindée d'émojis. Ok.

### Les investisseurs

Pour moi, la bascule, ça a été au moment où il fut question d'ouvrir l'école à des "investisseurs". Sur le coup je ne comprenais pas pourquoi il fallait **croître d'avantages**, croître toujours plus, alors que l'école, d'après ce qu'on en ressentait, se portait bien. Mais bon, chacun son taf. Nous, nous sommes de simples profs, on taf dure, et "ils" sont les capitaines d'un bateau qu'ils connaissent bien. Enfin, on l'espère.

Malheureusement, ces doutes quant aux réels objectifs de ces investisseurs n'ont pas été dissipés par le CEO, malgré ses nombreuses visioconférences. Au contraire. On se demandait "mais pourquoi faut-il faire appel à des investisseurs qui seront étrangers à la culture (sympa et humaine) de l'entreprise ?".

Hélas, peu importe nos questions de "simples profs" posées pendant ces visioconférences, la décision était prise. Et elle était indiscutable, malgré les apparences. Je crois que c'était la première fois que je comprenais que, finalement, nous n'avions rien à dire. Juste à acquiescer. Je n'avais pas ressenti une telle verticalisation dans la prise de décision jusque là. J'étais loin de me douter que ce n'était que le début.

### Plein de thunes, donc plein de nouveaux profs et tuteurs

Les sous ont permis de recruter plein de nouveaux profs, et plein de nouveaux encadrants de promos. Super. Des nouveaux collègues. Et bien sympas apparemment en plus. Parfait.

### Des promos qui gonflent

*"Il faut grossir"*. Et les promos grossissent aussi. Donc les salles de classes vont enfler. On parle de grosse promo séparée en trois grandes salles de classes.

### Ah zut, on a oublié qu'il fallait d'abord plus d'élèves

Premier gros fail : on a plein de nouveaux profs et encadrants, on a de quoi avoir de plus grosses classes, mais il semblerait que la direction ait oublié qu'il fallait y mettre plus d'élèves. Donc plus d'élèves à trouver. La direction dit "on manque d'élèves", là on nous disons "mais on ne manquait pas d'élèves avant la décision d'agrandir les classes".

### Le responsable pédagogique est manquant

Oops, le responsable pédagogique a fait ses valises. Ce type, si humain, si sensible à la vraie qualité de cours, bah il s'est cassé.

### C'est le bordel

D'autres formateurs font leur valise. Dans la boîte, c'est le dawa. Il faut gérer les absences des uns, remplacer, maintenir la qualité, améliorer les supports, répondre aux questions des nouveaux profs. L'école est en roue libre. Plus personne ne tient le gouvernail de la "stratégie pédagogique". Malgré tout, l'équipe pédagogique de terrain assure, comme d'hab, grace à l'entraide, la fraternité et l'humanité qui habitent encore les lieux.

### Un nouveau responsable/directeur pédagogique

Un nouveau responsable / directeur pédagogique est recruté. Nous, on taf comme des cinglés pour que la boîte tourne. Pas vraiment le temps de comprendre pourquoi ce nouveau responsable / directeur pédagogique ne se présente pas à nous. Pas vraiment le temps d'écouter les premières alertes quant à son manque total d'empathie ressentis par certain et certaines.

### On ne sait pas si on touchera nos salaires

C'est la crise. Pour une raison qui échappe aux profs, la direction annonce qu'elle ne sait pas si les salariés pourront toucher leurs salaires dans les mois qui suivent.

Là, ça commence à tourner vinaigre. On taf comme des cinglés à cause de décisions sur lesquelles on ne peut rien dire. Et maintenant, on apprend qu'on est proche de la faillite. Oklm. *"Tout est fait pour éviter de licencier des salariés"*, nous dit-on. Mais bon, *"pas sûr"*.

### Communication interne désastreuse

Puis on nous dit : certains formateurs seront peut-être virés. "Mais pas sûr." Nous on pense : quel enfer. Puis on nous demande si on est marié. Si on a des enfants. Ou si on est seul au foyer avec enfants. Plus on coche de critères, moins on a de chance d'être virés. Mais c'est quoi ce bordel ?

### Les développeurs sont virés

On est une école qui forme des développeurs. Mais les développeurs internes de l'école, qui travaillent à plein temps sur nos outils du quotidien, et qui les améliorent... Bah ces développeurs internes sont tous virés. La direction vient de le dire. C'est pour éponger la crise de la boîte.

### Un outil de cours en visio, en moins bien, et tout buggé

On met en avant le prochain outil de cours en visio, et on va être obligé de l'utiliser. Manque de bol, les développeurs ne sont plus là pour le débugger. Et qu'est-ce qu'il est buggé d'ailleurs. Je retourne sur l'ancien outil, beaucoup plus stable.

### Mise en place des contremaîtres/managers

Pour mieux "encadrer les équipes", la direction met en place des managers. Des anciens formateurs qui tirent leur épingle du jeu (bravo à eux) et qui deviennent soudainement des contremaîtres. Des anciens formateurs qui vont gérer des formateurs.

> Forcément, ils pensent aussi à leurs carrière ...

Désormais on ne parle plus à qui que ce soit de la direction. On doit parler avec notre "manager". La pyramide se renforce. Exit l'entreprise sympa, fraîche, humaine, où tout le monde se parle, tout le monde échange, avec tout le monde. Les formateurs sont divisés en sous-groupes, dans des "teams", chapeautées par ces managers.

### Ce n'est plus une belle école, c'est une vilaine usine

Beaucoup de temps est passé ; les formateurs parlent avec leurs managers dans des réunions hebdomadaires obligatoires. Mais ces "réunions" deviennent des sortes de journaux télé. Certains managers y sont des espèces d'animateurs, qui présentent les dernières décisions de la direction.

Parfois les formateurs formulent quelques observations qui ne vont pas dans le sens des décisions de la direction. Ces observations sont notées dans des documents sauvegardés dans un "drive". Et puis, plus rien.

Parfois certains managers essayent de faire remonter des observations de formateurs. Mais ils reviennent bredouille. En les travaillant au corps, on apprendra de leur bouche que "la direction n'écoute pas".

### Le niveau des apprenants est en chute libre

Pendant ce temps, les formateurs travaillent de plus en plus dur. Car le niveau requis, pour devenir un étudiant de notre école, a été radicalement revu à la baisse. Les vannes sont grandes ouvertes. L'école semble aspirer tout ce qui traîne. Certains apprenants n'ont pratiquement jamais touchés à un ordinateur. Ce ne sont plus les apprentis codeurs d'avant.

Manque de bol, nos programmes ont un rythme ultra soutenu. Un rythme déjà compliqué pour un apprenti codeur. Alors pour des apprenants qui n'ont jamais codé de leur vie, je te passe les détails.

Certains collègues n'hésitent pas à traiter ces étudiants en grande difficulté de "boulets". De dire "qu'ils n'ont rien à faire là". Moi je préfère plutôt réfléchir aux façons de rattraper ces apprenants noyés dans le flux massif d'informations. Je commence à réfléchir à des systèmes de programmes où les profs pourraient librement piocher les exercices corrigés et les supports explicatifs d'une notion donnée. J'essaye de proposer mon idée au responsable / directeur pédagogique. Qui me demande sèchement de voir ça avec mon manager. Manager qui ne comprend pas la réponse du responsable / directeur pédagogique. Mais je lâche pas, et continue de pousser mon idée à d'autres collègues qui ont la main sur les programmes. On me dit que ce que je propose "existe déjà". Qu'ils "y travaillent". Mais, 1 an plus tard, force est de constater que ce qui "existe déjà" n'est toujours pas en place.

### Ce n'est plus une école, c'est une usine. Nous en sommes désormais les ouvriers

Au fil du temps je me rends compte que de moins en moins de personne se salue dans les canaux publics de l'outil de communication de la boîte. On croule sous le travail. Je vois d'anciens formateurs, pourtant expérimentés, tellement fatigués. D'autres sont déprimés. Certains se mettent en arrêt. Les décisions de la direction font que nous n'avons plus le temps de préparer correctement les cours. Le problème a été remontés d'innombrables fois. Mais la direction reste sourde à nos appels. Pire, elle traite de "divas" les formateurs qui se plaignent de leurs conditions de travail qui sont, pourtant, en constante détérioration.

> Je me reconnais bien dans ce que tu dis, en particulier les "ouvriers". J'ai l'impression qu'on est pas du tout reconnu, qu'un formateur ou un autre, freelance ou pas freelance, ça revient au même.

> Si tu es trop souvent contre les décisions de la direction, elle ne t'écoutera plus.

> Avoir si peu de temps de préparation des cours, dans les faits, c'est compatible avec personne. Ça force même certain et certaines à préparer la nuit et le week-end. Comme un freelance.

> Ah ouais, toi aussi tu vois des collègues épuisés ? C'est pas beau à voir hein ? Les managers tentent d'éponger et la direction est toujours en mode "rien à foutre" ... Qu'on ne vienne pas dire que la restructuration va changer cela.

### Uberisation

La direction annonce qu'elle va faire appel à des freelances. Certains managers disent que, si on veut, nous, les formateurs, on pourrait quitter notre CDI pour devenir freelance. Puis rétro pédalage aux réunions suivantes. Ça ne sera pas possible. Enfin si. Mais sous conditions

Malheureusement, le salaire des freelances est bien en dessous des moyennes actuelles. Et s'il faut 1 à 2 jours de préparation de cours, pour 1 jour de cours donné, le salaire proposé à un freelance devient carrément ridicule.

> Si je veux faire davantage de conception, il faut que je démissionne pour devenir freelance ?

(À cette question, la direction répondra : **oui**)

> Le "dialogue" pour la direction, c'est "partez si ça ne vous va pas"

> Dans une entreprise le truc principal et essentiel, c'est la motivation des salariés. Avec la motivation on déplace des montagnes. Sans la motivation, on ne fait rien du tout. Et le pire c'est que la démotivation est très contagieuse dans les équipes.

> L'école aurait-elle changée ses valeurs ? Après "les investisseurs", voici l'uber, et l'argent d'uber.

> Très ironiquement, j'ai envie de dire que la démotivation n'est pas contagieuse dans une entreprise, si tout le monde est en freelance et se côtoie maximum 3 semaines par an. Et pour aider, la direction fabriquera du turn over.

> Les livreurs en freelances n'ont souvent pas d'autres possibilités d'emplois. Alors que les formateurs que nous sommes ont également les compétences des développeurs...

### Un CSE passe-plat

La direction annonce de nouveaux changements organisationnels. Ces changements sont relayés par le CSE. La direction joue sur les mots. Proposition de restructuration. Projet. Discussion. Ébauche. J'ai l'impression que le CSE se fait avoir et ne fait finalement que le jeu de la direction.

Je me souviens lorsque j'étais secrétaire CHSCT d'une grosse boîte (avant la fusion CHSCT + CE donnant l'entité CSE), j'avais à cœur d'être au plus proche des salariés, peu importe leur niveau hiérarchique. J'allais les voir, un par un, séparément, et en créant une relation de confiance. J'obtenais des informations cruciales, sensibles, parfois confidentielles, et je m'appliquais à les anonymiser, et à les faire remonter une par une à la direction. Chaque question, chaque problème soulevé formait un des points des ordres du jours. Et tant que le point n'était pas résolu, je prenais soin de l'inscrire inlassablement dans les ordres du jour suivants. Et je notais mot à mot les réponses de la direction, qui ne manquait jamais d'essayer de noyer le poisson.

Bon dans le cas de notre école, j'ai l'impression qu'on est encore loin de ça.

### L'usine où les nouvelles idées sont tuées dans l'oeuf

Je te le disais tout à l'heure, j'ai essayé de proposer de nouvelles idées pour améliorer certaines choses dans la boîte. Voici comment ça s'est terminé :

<details>
  <summary>Cas n°1 : "le serpent se mord la queue"</summary>

  ![cheminement d'une proposition- cas n°1](docs/cheminement-cas-1.png)

</details>

<details>
  <summary>Cas n°2 : "tout ça, pour ça"</summary>

  ![cheminement d'une proposition- cas n°2](docs/cheminement-cas-2.png)

</details>

### Le mot de la fin

Et beh voilà. Salut à toi copain. Salut à toi copine. Supers formateurs, supers formatrices, supers tuteurs, supers tutrices, supers ingénieurs pédagogiques et chouettes personnes que j'ai côtoyé. Je suis tellement triste parce que je sais que *tout est là*. Toutes les compétences des équipes sont là. Et c'est avec beaucoup de chagrin que je rejoins celles et ceux qui ont quitté le navire. Mais y'a trop de trous dans la coque. Et quand ce sont les capitaines qui percent le bateau, c'est irrationnel.

### Vos mots de la fin

> Bonne continuation ! L'école perd encore un bon membre de son équipe, mais pas d'inquiétude, tu seras remplacé par 3 freelances différents à qui on passera les clés le matin même, juste avant leur cours en visio.

> Je suis un peu triste que tu t'en ailles, mais je pense que tu as bien raison...

> Si tu demandes le poste du responsable / directeur pédagogique, je suis sur que t'auras le soutien nécessaire pour l'obtenir.

> Hello j'ai une petite question tu as eu une rupture conventionnelle ? C'est pour un ami.

### Le mot de ChatGPT

Tu es un sociologue. Tu écris un livre sur les méfaits de la hierarchisation à outrance dans les entreprises, où les salariés ne peuvent plus proposer de changements pouvant pourtant s'avérer très positifs pour l'entreprise.

> Dans ce livre, je pourrais explorer les effets négatifs de la rigide hiérarchisation des entreprises sur la motivation et la créativité des employés. Je pourrais discuter des manières dont les structures de pouvoir rigides peuvent inhiber la capacité des employés à faire des propositions de changement, même lorsqu'ils ont des idées qui pourraient s'avérer bénéfiques pour l'entreprise. Je pourrais également aborder les conséquences à long terme de cette situation sur la culture d'entreprise, la productivité et la satisfaction au travail.
>
> Je pourrais également inclure des études de cas concrètes de sociétés qui ont adopté des approches plus horizontales et participatives, ainsi que les avantages et les défis de ce genre de modèle. Finalement, je pourrais proposer des solutions pour renverser cette tendance, telles que des programmes de formation pour les gestionnaires et la promotion d'une culture d'entreprise ouverte et inclusive.

## Musique

- [Jusqu'à ce que la force de t'aimer me manque](https://youtu.be/4caisMiT3zI) (YouTube) - [ref](https://www.discogs.com/fr/master/205382-Catherine-Ribeiro-Alpes-Paix)

## Rideau de fin

On garde le contact ?

```
jeremie
```

arobase

```
humanize
```

point

```
me
```
